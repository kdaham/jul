CD ..
	
SET SCHEMADIR=..\schemas
	
SET W1=%SCHEMADIR%\interactions\ProcessClaimSpecificationInteraction\ProcessClaimSpecificationInteraction_1.0_RIVTABP21.wsdl
SET X1=%SCHEMADIR%\interactions\ProcessClaimSpecificationInteraction\*.xsd

SET XCORE=%SCHEMADIR%\core_components\*.xsd

SET SCHEMAS=%XCORE% %W0% %X0% %W1% %X1% %W2% %X2% %W3% %X3%

SET OUTFILE=/out:wcf\generated-src\JuL.cs
SET APPCONFIG=/config:wcf\generated-src\app.config
SET NAMESPACE=/namespace:*,JuL.v1
SET SVCUTIL="svcutil.exe"
%SVCUTIL% /language:cs %OUTFILE% %APPCONFIG% %NAMESPACE% %SCHEMAS%

CD wcf
