<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/testsuite">
		<html>
			<head>
		
			</head>
			<body>
				<h1>TK-testsuite for <xsl:value-of select="id"/></h1>
				
				<h2>About the testsuite</h2>
				<p>This document describes the testsuite for <xsl:value-of select="id"/>. The testsuite contains a number of testcases used to verify the implementation prior to integration with the national platform.<br/>
				The testsuite uses SoapUI to verify the implementation. Documentation of SoapUI can be found at <a target="blank" href="https://www.soapui.org/getting-started/introduction.html">www.soapui.org</a>.<br/>
				Click <a target="blank" href="https://www.soapui.org/downloads/soapui.html">here</a> to download SoapUI. Install according to documentation.</p>
				
				<h2>Setup instructions</h2>
				<p>
					<ul>
						<li>Locate the directory <b>test-suite/<xsl:value-of select="contractName"/></b> in the distribution</li>
						<li>Copy the file <b>‘soapui-support-N.N.NN.jar’</b> (where 'N.N.NN' is the latest version) to the folder <b>/bin/ext</b> where Soap-UI is installed (look in the folder 'Program Files/Smartbear')</li>
						<li>Open SoapUI and import the SoapUI project (<b><xsl:value-of select="contractName"/>-soapui-project.xml</b>) from the above directory (choose ‘Import Project’ from the File-menu)</li>
						<li>If your WebService endpoint requires a SSL Certificate, this can be configured from the Preferences (in the File menu). 
						In the Preferences window open the ‘SSL Settings’ tab and import the Keystore containing the Client Certificate</li>
						<li>Update <i>data.xml</i> to match the test-data contents in your system. If you have no suitable test-data, this need to be added in the source system (see the following section for instructions)</li>
						<li>You should now be able to run the testcases</li>
					</ul>
				</p>
				
				<h2>Testdata in <i>data.xml</i></h2>
				<p>
					Prior to running the testcases in SoapUI, the data used in requests must be adjusted to conform to the current system under test. This can be done by editing the file <i>data.xml</i> as described below.<br/>
					<br/>
					In the beginning of the document is a section called "globaldata". This is where you specify data that is used by all testcases.<br/>
					Each element in "globaldata" can be overridden in a specific testcase if needs be. The following elements are global:
					<ul>
						<xsl:for-each select="globaldata/*">
							<li>
								<xsl:value-of select="name()"/>
							</li>
						</xsl:for-each>
					</ul>
				</p>
				
				<h2>Testcase descriptions</h2>
				<p>The testcase-specific parameters completes and overrides parameters defined in 'globaldata'.<br/>
				What this means is that the parameters in globaldata and the testcase data-section are all used in the specific testcase.<br/>
				But if a parameter with the same name is defined both in globaldata AND the testcase, the definition from the testcase will be used.<br/>
				An example would be patientId, which is defined in globaldata, since it is probable that most test-data will have the same patientId.<br/>
				But for some testcases you will want to use a different patientId and for these you override it by un-commenting the parameter in the testcase data-section.</p>
				<ul style="list-style-type:none">
					<xsl:for-each select="testcase">
						<h3>
							<li>
								<xsl:value-of select="@id"/>
							</li>
						</h3>
						<p>
							<xsl:copy-of select="description"/>
						</p>
						<xsl:if test="data/*">
							<b>Testcase-specific parameters</b>
						</xsl:if>
						<ul>
							<xsl:for-each select="data/*">
								<li>
									<xsl:value-of select="name()"/>
								</li>
							</xsl:for-each>
						</ul>
					</xsl:for-each>
				</ul>
				<br/>
				<br/>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet> 